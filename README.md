# Exercice de test technique pour la formation CDA Simplon

La formation Concepteur Développeur d'Application vise à délivrer un titre équivalent BAC+3, avec un référentiel de compétences 
ambitieux à atteindre sur l'année. La formation a débuté début décembre, les candidats qui la rejoignent en janvier doivent absolument
disposer de prérequis technique en programmation Java, sinon ils n'arriveront pas à suivre le rythme de la formation.

Le but de cet exercice et de s'assuser que le candidat aura le niveau nécessaire afin de suivre le contenu de la formation à partir du mois
de janvier.


# Consignes 

Créer un fork de ce repo sur votre propre compte gitlab (que vous créerez si vous n'en avez pas déjà un).

Implémentez la partie 1 de l'exercice sur une branche 'release-part1'.

Implémentez la partie 2 de l'exercice sur une branche 'release-part2'.

Une fois l'exercice fait, vous pouvez envoyer l'adresse de votre repo Gitlab qui contient la solution.

# Partie 1

Il s'agit de compléter le code existant (il faudra décommenter les appels à `partA()` et `partB()` dans la méthode `main()`),
en suivant la description fonctionnelle du programme décrite ci dessous.

## Partie 1 - A

On veut représenter dans notre application des logements.

Un logement a un loyer mensuel, une adresse postale. On veut aussi pouvoir savoir pour chaque logement
le loyer annuel, et si un logement est accessible pour une personne à mobilité réduite.

Il y a deux sous types de logement, des maisons et des appartements. Les appartements ont un numéro d'étage
(mais pas les maisons).

Une maison est toujours accessible pour les PMR. Un appartement est accessible pour les PMR seulement s'il est au rez de chaussée.
(Les ascenceurs n'ont pas été inventés).


Créez les classes nécessaire, en mettant en place de l'héritage, afin de représenter les maisons et les appartement dans notre programme.
Déduisez du code existant les méthodes nécessaires au bon fonctionnement des classes.

Vous veillerez à bien respecter le principe d'encapsulation.


## Partie 1 - B 

On veut représenter des employés. Un employé a un salaire mensuel. On veut aussi connaitre le coût annuel d'un employé
(si on est son employeur), c'est à dire son salaire des 12 mois + les charges sociales qui sont 0.75 x le salaire (arrondi à l'euro près).

On veut représenter une entreprise. Une entreprise a un nom, et plusieurs employés. 
Elle loue également plusieurs habitations (pour ses bureaux).

On veut ensuite calculer le total des charges de l'entreprise sur une année (total des coût des employés + total des coût des habitations 
louées).

Implémentez les nouvelles classes afin de représenter les entreprises et les employés. Réutilisez les classe que vous aviez créées en première
partie pour les appartements et les maisons. Mettez en place une relation d'association entre la classe qui représente une entreprise, et les classes
représentant des salariés et des logements.

Comme pour la partie A, appuyez vous sur le code existant pour déduire les méthodes à implémenter.

# Partie 2


Supprimez l'implémentation du `main()` fournie pour la partie 1, et remplacez la par votre implémentation pour résoudre le problème de la partie 2.

On va seulement réutiliser notre classe qui représente les employés.

Nous avons dans le dossier `./data` une base de données SQLite `employees.sqlite`.  Voici le schéma de cette base de données : 

```
CREATE TABLE sqlite_sequence(name,seq);
CREATE TABLE IF NOT EXISTS "employees" (
	`name`	TEXT NOT NULL,
	`salary`	INTEGER NOT NULL,
	`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE
);
```

C'est donc une base de données assez simple, avec une seule table `employees`, table qui contient 3 colonnes `name`, `salary` et `id`.


Vous devez écrire votre programme Java, afin que celui ci lance un serveur HTTP local, sur le port 3014. Ce serveur HTTP doit répondre aux requêtes
suivantes :

- `GET /employees`
- `GET /employees/:id` où `:id` est n'importe quel entier valide

Pour répondre à ces requêtes, le programme doit alors lire la base de données SQLite afin d'extraire les données des employés qui y sont stockées,
et renvoyer les données en réponse à la requête HTTP, au format JSON.

Vous ne devez pas utiliser de framework tel que Spring. Utilisez JDBC pour l'accès à la base de données. Vous pouvez utiliser 
`HttpServer` fourni par la package com.sun.net.httpserver  pour créer le serveur HTTP. 
Si vous préférez utiliser une autre librairie qui permette de créer un serveur HTTP vous pouvez.


## Détail de l'implémentation attendue pour les endpoint de votre API HTTP

### GET /employees

Retourne la liste de tous les employés présents dans la base de données.

Format de la réponse : 

```
[{
  "id": 1,
  "name": "John",
  "salary": 10
}, {
  "id": 2,"
  "name": "Bob",
  "salary": 11
},
...
] 
```

### GET /employees/:id

Renvoit les information de l'employé de l'ID demandé.

Exemple de requête valide `GET /employees/1`

Format de la réponse : 

```
{
  "id": 1,"
  "name": "John",
  "salary": 10
}
```

## Outils

Pour explorer facilement la base de données SQLite, vous pouvez utiliser le client SQLiteBrowser.

Pour tester votre API HTTP, vous pouvez utiliser le client Postman, ou la ligne de commande `curl`.

Avec la base de données fournie, si votre programme fonctionne selon la consigne, les commande curl suivantes devraient vous 
donner ces résultats :

```
$ curl http://localhost:3014/employees
[{
  "id": 1,
  "name": "Alice",
  "salary": 2000,
}, {
  "id": 2,
  "name": "Bob",
  "salary": 1000,
}, {
  "id": 3,
  "name": "Charlie",
  "salary": 1500,
}]
```

```
$ curl http://localhost:3014/employees/2
{
  "id": 2,
  "name": "Bob",
  "salary": 1000,
}
```

